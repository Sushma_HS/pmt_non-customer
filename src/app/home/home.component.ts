import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LoginModelComponent } from './../Model/login-model/login-model.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private title = "Kannan Tyres"

  constructor(public dialog: MatDialog) { }

    openDialog(): void {
    const dialogRef = this.dialog.open(LoginModelComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit() {
  }

}
