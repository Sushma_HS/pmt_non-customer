
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginModelComponent } from './Model/login-model/login-model.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { RegFormComponent } from './retailers/reg-form/reg-form.component';
import { RetailerListComponent } from './retailers/retailer-list/retailer-list.component';
import { EditRetailerComponent } from './retailers/edit-retailer/edit-retailer.component';
import { CountryComponent } from './country-list/country.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { EditCountryComponent } from './edit-country/edit-country.component';
import { AddCountryComponent } from './add-country/add-country.component';
import { CategoryComponent } from './general/category/category.component';
import { CompanyComponent } from './general/company/company.component';
import { ItemsComponent } from './general/items/items.component';
import { CataddComponent } from './general/catadd/catadd.component';
import { CompanyaddComponent } from './general/companyadd/companyadd.component';
import { ItemsaddComponent } from './general/itemsadd/itemsadd.component';
import { CateditComponent } from './general/catedit/catedit.component';
import { CompanyeditComponent } from './general/companyedit/companyedit.component';
import { ItemseditComponent } from './general/itemsedit/itemsedit.component';
import { RolesComponent } from './general/roles/roles.component';


const routes: Routes = [

    //  Site routes goes here 
    { path:'adminDashboard',component:AdminDashboardComponent},
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    { path:'home',component:HomeComponent},
   {
     path: 'reg-cmp',
     component:RegFormComponent 
   },
   {
    path: 'list-cmp',
    component:RetailerListComponent 
  },
  {
    path: 'edit-cmp',
    component:EditRetailerComponent 
  },
  {
    path: 'country-cmp',
    component:CountryComponent
  },
  {
    path: 'dropdown-cmp',
    component:DropdownComponent
  },
  {
    path: 'edit-country-cmp',
    component:EditCountryComponent
  },
  {
    path: 'add-country',
    component:AddCountryComponent
  },
  { path:'category', component:CategoryComponent },
    { path:'company',component:CompanyComponent},
    { path:'items',component:ItemsComponent},
    { path:'catadd',component:CataddComponent},
    { path:'companyadd',component:CompanyaddComponent},
    { path:'itemsadd',component:ItemsaddComponent},
    { path:'catedit',component:CateditComponent},
    { path:'companyedit',component:CompanyeditComponent},
    { path:'itemsedit',component:ItemseditComponent},
    { path:'roles',component:RolesComponent}
    // { path:'**',component:PageNotFoundComponent}
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule {HomeComponent};
  export const routingComponents = [LoginModelComponent];
  
