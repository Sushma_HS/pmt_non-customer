import { Component, OnInit } from '@angular/core';
import { CountryNamesService} from '../shared/country-names.service';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormControl,FormBuilder } from '@angular/forms';
import { Country } from '../country-list/country.model';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {

  createForm:FormGroup;
  constructor(private countrynamesService: CountryNamesService,private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createForm=this.formBuilder.group({
      id:new FormControl('',[Validators.required]),
         name:new FormControl('',[Validators.required]) 
          });
  }
  onSubmit() {
    this.countrynamesService.createCountry(this.createForm.value)
      .subscribe( data => {
        this.router.navigate(['country-cmp']);
      });
  }

}
