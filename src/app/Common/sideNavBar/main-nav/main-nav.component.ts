import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {}

  goitems() {
    this.router.navigate(['item-cmp']);
  }
  goreg() {
    this.router.navigate(['reg-cmp']);
  }
  golist() {
    this.router.navigate(['list-cmp']);
  }
  goedit(){
    this.router.navigate(['edit-cmp']);
  }
  gocount(){
    this.router.navigate(['country-cmp']);
  }
  gocsc(){
    this.router.navigate(['dropdown-cmp']);
  }
  gocategory(){
    this.router.navigate(['category']);
  }
  company(){
    this.router.navigate(['company']);
  }
  items(){
    this.router.navigate(['items']);
  }
  roles(){
    this.router.navigate(['roles']);
  }
}
// links
// https://stackoverflow.com/questions/56054770/how-to-create-a-nested-menu-using-angular-mat-nav-material-updated
