import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-model',
  templateUrl: './login-model.component.html',
  styleUrls: ['./login-model.component.css']
})
export class LoginModelComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<LoginModelComponent>,private router: Router,@Inject(MAT_DIALOG_DATA) public data: any) { }


  hideModel() {
    this.dialogRef.close("Closed");
  }

  login(){
    this.dialogRef.close("Closed");
    this.router.navigate(['adminDashboard'])
    }

  ngOnInit() {
  }

}
