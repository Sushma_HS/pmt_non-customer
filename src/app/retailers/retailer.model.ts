export class Retailer {
    id: number;
    firstName: string;
    lastName: string;
    country: string;
    state: string;
    city: string;
    address: string;
    gstNumber: any;
    panNumber: any;
    bankName: string;
    accountNumber: any;
    ifsccode: any;
}
