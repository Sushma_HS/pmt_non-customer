import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { RetailersService } from '../../shared/retailers.service';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/notification.service';
import { DialogService } from 'src/app/shared/dialog.service';
import { Retailer } from '../retailer.model';



@Component({
  selector: 'app-retailer-list',
  templateUrl: './retailer-list.component.html',
  styleUrls: ['./retailer-list.component.css']
})
export class RetailerListComponent implements OnInit {

  retailers: Retailer[];
 
  constructor(private service: RetailersService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private router: Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['Name', 'gstNumber', 'panNumber', 'bankName', 'accountNumber','ifsccode','actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  
  ngOnInit() {  
    this.service.getEmployees()  
      .subscribe((data: Retailer[]) => {  
        this.retailers = data;  
      });  
  } 
  onCreate(){
    this.router.navigate(['reg-cmp']);
  } 
  deleteEmp(retailer: Retailer): void {  
    this.service.deleteEmployees(retailer.id)  
      .subscribe(data => {  
        this.retailers = this.retailers.filter(u => u !== retailer);  
      })  
  }  
  editEmp(retailer: Retailer): void {  
    localStorage.removeItem('editEmpId');  
    localStorage.setItem('editEmpId', retailer.id.toString());  
    this.router.navigate(['add-emp']);  
  }  
}
