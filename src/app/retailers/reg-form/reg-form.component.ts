import { Component, OnInit } from '@angular/core';
import { RetailersService } from '../../shared/retailers.service';
import { NotificationService } from '../../shared/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.css']
})
export class RegFormComponent implements OnInit {

  constructor(private service: RetailersService,
              private router: Router) { }
              ngOnInit(){
                let empid = localStorage.getItem('editEmpId');  
                if (+empid > 0) {  
                  this.service.getEmployeeById(+empid).subscribe(data => {  
                    this.service.form.patchValue(data);  
                  })  
                  
                }  
              }
  
  onSubmit() {  
    console.log('Create fire');  
    this.service.createUser(this.service.form.value)  
      .subscribe(data => {  
        this.router.navigate(['list-emp']);  
      },  
      error => {  
        alert(error);  
      });  
  }  
  onUpdate() {  
    console.log('Update fire');  
    this.service.updateEmployee(this.service.form.value).subscribe(data => {  
      this.router.navigate(['list-emp']);  
    },  
      error => {  
        alert(error);  
      });  
  }  
}  

      // onSubmit(){
      //   if(this.service.form.valid){
      //     this.service.insertRetailer(this.service.form.value);
      //     this.service.form.reset();
      //     this.service.initializeFormGroup();
      //     this.notificationService.success(':: Submitted successfully');
      //   }
      // }
      
  
  
  
