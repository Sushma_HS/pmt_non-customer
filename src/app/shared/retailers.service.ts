import { Injectable } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Retailer } from '../retailers/retailer.model';
// import { Retailer } from '../retailers/retailer.model';



@Injectable({
  providedIn: 'root'
})
export class RetailersService {

    form:FormGroup  = new FormGroup({
    id: new FormControl(null),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('',Validators.required),
    country: new FormControl('',[Validators.required]),
    state: new FormControl('',[Validators.required]),
    city: new FormControl('', [Validators.required]),
    gstNumber: new FormControl('',[Validators.required, Validators.minLength(15)]),

    panNumber: new FormControl('',[Validators.required, Validators.minLength(10),Validators.pattern(/[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}/)]),
    address: new FormControl('',Validators.required),
    bankName:new FormControl('',Validators.required),



   accountNumber: new FormControl('',[Validators.required, Validators.minLength(16)]),
   ifsccode: new FormControl('',[Validators.required,Validators.minLength(11),Validators.pattern(/^[A-Za-z]{4}0[A-Z0-9]{6}$/)])

 });


 initializeFormGroup() {
  this.form.setValue({
    $key: null,
    firstName: '',
    lastName: '',
    country: '',
    state: '',
    city: '',
    gstNumber: '',
    panNumber: '',
    address: '',
    bankName: '',
    accountNumber:'',
    ifsccode:''
  });
}

constructor(private http: HttpClient) { }  
baseUrl: string = 'http://localhost:3004/retailers';  

getEmployees() {  
  return this.http.get<Retailer[]>(this.baseUrl);  
}  
deleteEmployees(id: number) {  
  return this.http.delete<Retailer[]>(this.baseUrl+'/' + id);  
}  
createUser(retailer: Retailer) {  
  return this.http.post(this.baseUrl, retailer);  
}  
getEmployeeById(id: number) {  
  return this.http.get<Retailer>(this.baseUrl + '/' + id);  
}  
updateEmployee(retailer: Retailer) {  
  return this.http.put(this.baseUrl + '/' + retailer.id, retailer);  
}  

}
