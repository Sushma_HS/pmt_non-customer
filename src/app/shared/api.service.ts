 import { Injectable } from '@angular/core';
 import { HttpClient, HttpClientModule, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
 
 import { Observable, throwError, from } from 'rxjs';
 import { retry, catchError } from 'rxjs/operators';
 import { CategoryComponent } from '../general/category/category.component';
 import { CataddComponent } from '../general/catadd/catadd.component';
import { Category1 } from '../general/category/category.model';

 @Injectable({
   providedIn: 'root'
 })
 export class ApiService {
   category1: any;
   category: any;
   id(id: any) {
     throw new Error("Method not implemented.");
   }
   Id(Id: any) {
     throw new Error("Method not implemented.");
   }
   initializeFormGroup() {
     throw new Error("Method not implemented.");
   }
   form: any;
   delete(id: number) {
     throw new Error("Method not implemented.");
   }
   http: any;

  constructor(private httpClient:HttpClient) { }
  baseUrl:string="http://192.168.0.137:8088/api";

  getcategory(){
    return this.httpClient.get<Category1[]>(this.baseUrl+'/getCategories');
   }
   deleteCategory(id:number){
 return this.httpClient.delete<Category1[]>(this.baseUrl+'/'+id);
   }
   createUsers(category1:Category1)
   {
     return this.httpClient.post<Category1[]>(this.baseUrl,category1);
   }
   getCategorById(id:number)
   {
 return this.httpClient.get<Category1[]>(this.baseUrl+'/'+id);
   }
   updateCategory(category1:Category1)
   {
 return this.httpClient.put<Category1[]>(this.baseUrl+'/'+ category1.id,category1);
   }
     
  
// updateItem(item:CataddComponent): Observable<CataddComponent> {
//   return this.http
//     .put<CataddComponent>(this.base_path + '/' + CataddComponent,item )
  
// }

// // // Delete item by id
// // deleteItem(id:number) {
// //   return this.http
// //     .delete<Student>(this.base_path + '/' + id)
   
// // }
// // getItem(id): Observable<Student> {
// //   return this.http
// //     .get<Student>(this.base_path + '/' + id)
  
// // }

// // Get students data
// getList(): Observable<CataddComponent> {
//   return this.http
//     .get<CataddComponent>(this.base_path)
  
// } 
 
 }
