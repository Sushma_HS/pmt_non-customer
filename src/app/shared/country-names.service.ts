import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../country-list/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountryNamesService {

  private baseUrl = 'http://192.168.0.183:8088/api';

  constructor(private httpClient: HttpClient) { }

  // getCountryList(id: number): Observable<Object> {
  //   return this.http.get(`${this.baseUrl}/${id}`);
  // }
  createCountry(country1:Country) {
    return this.httpClient.post(this.baseUrl+'/createCountry', country1);
  }

  getCountryList(): Observable<any> {
    return this.httpClient.get<Country>(this.baseUrl+'/getCountries');
  }
  deleteCountry(id:number){
    return this.httpClient.delete<Country>(this.baseUrl+'/'+id);
  }
  getCountryById(id:number)
  {
return this.httpClient.get<Country>(this.baseUrl+'/getcountry'+id);
  }
  updateCountries(country1:Country)
  {
return this.httpClient.put<Country>(this.baseUrl+'/'+ country1.id, country1);
  }
}
