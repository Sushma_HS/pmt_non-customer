import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
 
 import { Observable, throwError, from } from 'rxjs';
 import { retry, catchError } from 'rxjs/operators';
 import { CompanyComponent } from '../general/company/company.component';
 import { CompanyaddComponent } from '../general/companyadd/companyadd.component';
import { Company1 } from '../general/company/company.model';

@Injectable({
  providedIn: 'root'
})
export class Api1Service {
  company1: any;
  company: any;
  id(id: any) {
    throw new Error("Method not implemented.");
  }
  Id(Id: any) {
    throw new Error("Method not implemented.");
  }
  initializeFormGroup() {
    throw new Error("Method not implemented.");
  }
  form: any;
  delete(id: number) {
    throw new Error("Method not implemented.");
  }
  http: any;

  constructor(private httpClient:HttpClient) { }
  baseUrl:string="http://localhost:3200/company";

  getcompany(){
    return this.httpClient.get<Company1[]>(this.baseUrl);
   }
   deletCompany(Company_id:number){
 return this.httpClient.delete<Company1[]>(this.baseUrl+'/'+Company_id);
   }
   createUsers(company1:Company1)
   {
     return this.httpClient.post<Company1[]>(this.baseUrl,company1);
   }
   getCompanyById(Company_id:number)
   {
 return this.httpClient.get<Company1[]>(this.baseUrl+'/'+Company_id);
   }
   updateCompany(company1:Company1)
   {
 return this.httpClient.put<Company1[]>(this.baseUrl+'/'+ company1.Company_id,company1);
   }
}
