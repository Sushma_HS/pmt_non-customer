import { Component, OnInit } from '@angular/core';
import { Category1 } from './category.model';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/api.service';
import { from } from 'rxjs';

import { MatButton } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  category1:Category1[];

  // getCategorById: any;
  // getcategory: any;
  // getCategory: any;
  // getCategory1: any;

  //  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private apiService:ApiService, private router: Router) { }
  //  listData: MatTableDataSource<any>;
   displayedColumns: string[] = ['id', 'Category_name', 'Actions']


  ngOnInit(){
    // this.dataSource.paginator = this.paginator;
  
  
    this.apiService.getcategory()
    .subscribe((data:Category1[])=>{
      this.category1=data;
     var cat=this.category1;
      console.log(this.category1);
    });
  
}
 delete (category: Category1){
  //Update item by taking id and updated data object
   this.apiService.deleteCategory(category.id).subscribe(data => {
     this.category1=this.category1.filter(u => u !== category)
    //  this.router.navigate(['/category']);
   })
}
  
//(category1:Category1):void{
//   this.apiService.deleteCategory(category1.id)
//   .subscribe (data=>
//      {
//       this.category1=this.category1.filter(u =>u !==category1)
//     })

// catedit (category1:Category1):void
// {
//   localStorage.removeItem('cateditId');
//   localStorage.setItem('cateditId',category1.id.toString());
//   this.router.navigate(['catedit']);
  
// }
   
  catadd(){
    this.router.navigate(['catadd']);
  }
  
  catedit(category: Category1):void{
    localStorage.removeItem('catId');
    localStorage.setItem('catId',category.id.toString());
    this.router.navigate(['catedit']);
  }
  

}
