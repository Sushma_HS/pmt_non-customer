import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from '../../shared/api.service';
import { Category1 } from '../category/category.model';
import { getMatScrollStrategyAlreadyAttachedError } from '@angular/cdk/overlay/typings/scroll/scroll-strategy';

@Component({
  selector: 'app-catedit',
  templateUrl: './catedit.component.html',
  styleUrls: ['./catedit.component.css']
})
export class CateditComponent implements OnInit {
  
  editForm: FormGroup;
  category1: Category1;

  
  
  constructor(
    public activatedRoute: ActivatedRoute,private formBuilder:FormBuilder,private apiService:ApiService,
    public router: Router) { }


  ngOnInit() {
    let cat1id=localStorage.getItem('catId');
    if(!cat1id){
      alert("Invalid");
      return
    }
    this.editForm = this.formBuilder.group({
      id:[''],
      Category_name: ['', Validators.required]
    });
    this.apiService.getCategorById(+cat1id).subscribe(data=>{
      this.editForm.setValue(data);
    });

  }
  
   update() {
    console.log('Update Fire');
    this.apiService.updateCategory(this.editForm.value)
    .subscribe(data=>{
      this.router.navigate(['category']);
    },
    error=>{
      alert(error);
    });  
    
   
    }
   }
