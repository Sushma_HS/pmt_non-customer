import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Api1Service } from '../../shared/api1.service';

@Component({
  selector: 'app-companyadd',
  templateUrl: './companyadd.component.html',
  styleUrls: ['./companyadd.component.css']
})
export class CompanyaddComponent implements OnInit {
  addForm: FormGroup;
  constructor(
    public activatedRoute: ActivatedRoute,private formBuilder:FormBuilder,private api1Service:Api1Service,
    public router: Router) { }

  ngOnInit()  {
    this.addForm = this.formBuilder.group({
      Category_id: new FormControl("",Validators.required),
      Company_id: new FormControl(""),
      Company_name: new FormControl("")
    })
  }
  save() {
    //Update item by taking id and updated data object
     this.api1Service.createUsers(this.addForm.value).subscribe(response => {
       this.router.navigate(['/company']);
     })
  }

}

