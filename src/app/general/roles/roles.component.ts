import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


export interface RolesElement {
  Role_id:number; 
  Role_name: string;
 }
 

 const ELEMENT_DATA: RolesElement[] = [
   {Role_id: 1, Role_name: 'Admin'},
   {Role_id: 2, Role_name: 'Franchise'},
   {Role_id: 3, Role_name: 'Suppliers'},
   {Role_id: 4, Role_name: 'Employee'},
   {Role_id: 5, Role_name: 'Tie-Ups'},
   {Role_id: 6, Role_name: 'Operation Team'}
 ];

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  displayedColumns: string[] = ['Role_id', 'Role_name', 'Actions'];
  dataSource = ELEMENT_DATA;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  // catadd(){
  //   this.router.navigate(['catadd']);
  // }
  // catedit(){
  //   this.router.navigate(['catedit']);
  // }

}
