import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsaddComponent } from './itemsadd.component';

describe('ItemsaddComponent', () => {
  let component: ItemsaddComponent;
  let fixture: ComponentFixture<ItemsaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
