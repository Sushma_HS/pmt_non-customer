import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { ApiService } from '../api.service';
import { from } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'app-catadd',
  templateUrl: './catadd.component.html',
  styleUrls: ['./catadd.component.css']
})
export class CataddComponent implements OnInit {
 addForm: FormGroup;
  // category_id: number;
  // category_name: string;
 
  

  constructor(
    public activatedRoute: ActivatedRoute,private formBuilder:FormBuilder,private apiService:ApiService,
    public router: Router) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: new FormControl("",Validators.required),
      Category_name: new FormControl("")
    })
  }
  
  save() {
    //Update item by taking id and updated data object
     this.apiService.createUsers(this.addForm.value).subscribe(response => {
       this.router.navigate(['/category']);
     })
  }

}
