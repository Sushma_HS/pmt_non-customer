import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/api.service';
import { from } from 'rxjs';

import { MatButton } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Company1 } from './company.model';
import { Api1Service } from '../../shared/api1.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  company1:Company1[];
  company: any;
  getCompanyById: any;
  getcompany: any;
  getCompany: any;
  getCompany1: any; 
  // displayedColumns: string[] = ['Category_id', 'Company_id', 'Company_name', 'Actions'];
  //  dataSource = ELEMENT_DATA;

  constructor(private api1Service:Api1Service, private router: Router) { }
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['Category_id', 'Company_id', 'Company_name', 'Actions']

  ngOnInit() {
    this.api1Service.getcompany()
    .subscribe((data:Company1[])=>{
      this.company1=data;
    });
  
}
 delete (company: Company1){
  //Update item by taking id and updated data object
   this.api1Service.deletCompany(company.Company_id).subscribe(data => {
     this.company1=this.company1.filter(u => u !== company)
    //  this.router.navigate(['/category']);
   })
}
  
  companyadd(){
    this.router.navigate(['companyadd']);
  }
  companyedit(){
    this.router.navigate(['companyedit']);
  }


}
