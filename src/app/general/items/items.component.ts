import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface ItemsElement {
  Item_id:number; 
  Company_id:number;
  Item_name: string;

 }

 const ELEMENT_DATA: ItemsElement[] = [
  {Item_id: 1, Company_id:1, Item_name: 'Hydrogen'},
  {Item_id: 2, Company_id:2, Item_name: 'Helium'}
];

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  displayedColumns: string[] = ['Item_id', 'Company_id', 'Item_name', 'Actions'];
  dataSource = ELEMENT_DATA;

  constructor(private router: Router) { }

  ngOnInit() {
  }
  itemsadd(){
    this.router.navigate(['itemsadd']);
  }
  itemsedit(){
    this.router.navigate(['itemsedit']);
  }

}
