import { Component, OnInit } from '@angular/core';
import { CountryNamesService} from '../shared/country-names.service';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormControl,FormBuilder } from '@angular/forms';
import { Country } from '../country-list/country.model';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.css']
})
export class EditCountryComponent implements OnInit {

  createForm:FormGroup;
  constructor(private countrynamesService: CountryNamesService,private router: Router,
    private formBuilder: FormBuilder) { 
   
  }

  ngOnInit() {
    let cou1id=localStorage.getItem('editId');
    if(!cou1id)
    {
      alert("Invalid")
      // 
        // console.log(this.createForm);
      } 
      this.createForm=this.formBuilder.group({
        id:new FormControl('',[Validators.required]),
           name:new FormControl('',[Validators.required]) 
            });
      this.countrynamesService.getCountryById(+cou1id).subscribe(data=>{
          this.createForm.setValue(data);
  });
}
  

  onUpdate(){
    console.log('Update Fire');
    this.countrynamesService.updateCountries(this.createForm.value)
    .subscribe(data=>{
      this.router.navigate(['country-cmp']);
    },
    error=>{
      alert(error);
    });
  }

}
