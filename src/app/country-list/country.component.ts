import { Component, OnInit, ViewChild } from '@angular/core';  
import { Country } from './country.model';
import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { CountryNamesService } from '../shared/country-names.service';  
import { Router } from '@angular/router';



@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  countries: Country[];

  @ViewChild(MatPaginator) paginator: MatPaginator;  
  displayedColumns = ['id', 'name', 'action'];  

   

  
  constructor(public dataService: CountryNamesService, private router:Router) { 
    
  }  
  
  ngOnInit() {
    
    this.dataService.getCountryList().subscribe((data:Country[])=>
    {this.countries=data;
      // this.countries.paginator = this.paginator;
    });
    

    
  }
  
  deleteCount(country1:Country):void
{
  this.dataService.deleteCountry(country1.id)
  .subscribe(data=>{
    this.countries=this.countries.filter(u => u !== country1);
    // alert('Are u sure want to delete');
  })
}

editCount(country1:Country):void
  {
    localStorage.removeItem('editId');
    localStorage.setItem('editId',country1.id.toString());
    this.router.navigate(['edit-country-cmp']); 
  }
  onCreate(){
    this .router.navigate(['add-country'])

  }
}
