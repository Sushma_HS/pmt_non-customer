import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatDialogModule,MatSidenavModule,MatListModule, MatButtonModule, MatCardModule, MatFormFieldModule, MatMenuModule, MatTabsModule, MatToolbarModule, MatInputModule, MatTooltipModule, MatSnackBarModule,MatBadgeModule,MatStepperModule,MatTableModule,MatPaginatorModule, MatIconModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginModelComponent } from './Model/login-model/login-model.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeHeaderComponent } from './Layouts/home-header/home-header.component';
import { HomeFooterComponent } from './Layouts/home-footer/home-footer.component';
import { HomeLayoutComponent } from './Layouts/home-layout/home-layout.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { MainNavComponent } from './Common/sideNavBar/main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { RegFormComponent } from './retailers/reg-form/reg-form.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatSortModule} from '@angular/material/sort';
import { RetailersService } from './shared/retailers.service';
import { RetailerListComponent } from './retailers/retailer-list/retailer-list.component';
import { MatConfirmDialogComponent } from './retailers/mat-confirm-dialog/mat-confirm-dialog.component';
import { EditRetailerComponent } from './retailers/edit-retailer/edit-retailer.component';
import { HttpClientModule } from '@angular/common/http';
import { CountryComponent } from './country-list/country.component';
import { CountryNamesService } from './shared/country-names.service';
import { EditCountryComponent } from './edit-country/edit-country.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { CscService } from './shared/csc.service';
import { AddCountryComponent } from './add-country/add-country.component';
import { CategoryComponent } from './general/category/category.component';
import { CompanyComponent } from './general/company/company.component';
import { ItemsComponent } from './general/items/items.component';
import { CataddComponent } from './general/catadd/catadd.component';
import { CompanyaddComponent } from './general/companyadd/companyadd.component';
import { ItemsaddComponent } from './general/itemsadd/itemsadd.component';
import { CompanyeditComponent } from './general/companyedit/companyedit.component';
import { CateditComponent } from './general/catedit/catedit.component';
import { ItemseditComponent } from './general/itemsedit/itemsedit.component';
import { RolesComponent } from './general/roles/roles.component';
import { ApiService } from './shared/api.service';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginModelComponent,
    HomeHeaderComponent,
    HomeFooterComponent,
    HomeLayoutComponent,
    AdminDashboardComponent,
    MainNavComponent,
    RegFormComponent,
    RetailerListComponent,
    MatConfirmDialogComponent,
    EditRetailerComponent,
    CountryComponent,
    EditCountryComponent,
    DropdownComponent,
    AddCountryComponent,
    CategoryComponent,
    CompanyComponent,
    ItemsComponent,
    CataddComponent,
    CompanyaddComponent,
    ItemsaddComponent,
    CateditComponent,
    CompanyeditComponent,
    ItemseditComponent,
    RolesComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatBadgeModule,
    MatStepperModule,
    MatTableModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatListModule,
    MatSnackBarModule,
    MatFormFieldModule,
    RouterModule.forRoot(routes),
    LayoutModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatSortModule,
    HttpClientModule
    
  ],
  exports: [
    RouterModule,
    MatPaginatorModule,
    MatSortModule
  ],
  entryComponents:[
    LoginModelComponent,
    MatConfirmDialogComponent
  ],
  providers: [RetailersService, CountryNamesService, CscService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
